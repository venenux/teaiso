venenux-teaiso
==============

**The ISO creation tool for VenenuX development image cration**

## About teaiso-venenux

Image ISO live linux generation tool for **Alpine VenenuX edge** and **Debian VenenuX testing**.

This tool is useful for any user to create ISOs of alpine and debian, but focused on the testing 
branch of each flavor, however, it is not intended for users but for VenenuX developers, 
to know why and its purpose, read [doc/README.md - about the project](doc/README.md#about-the-project)

#### Changes and diferences

Those are fast reading changes.. but if you want to understand the changes check [doc/README.md - about the project](doc/README.md#about-the-project)

## Where to start

For DOCUMENTATION AND USAGE check [doc/Manual-of-usage.md](doc/Manual-of-usage.md)

## LICENSE

Check [LICENSE](LICENSE) for more information
