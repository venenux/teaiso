VenenuX Teaiso
==============

**The ISO creation tool for VenenuX development image creation**

This is an administrative level tool, so users without at least basic knowledge 
of linux will be disoriented. Are you ready to get into this boat? 🚢

## About Teaiso

This is a fork of the original project https://gitlab.com/tearch-linux/applications-and-tools/teaiso 
but customized for creating ISO live linux images of the VenenuX ISO images based on the
testing brands so will be **Alpine VenenuX edge** and **Debian VenenuX testing**.images.

Means this tool produces a testing Debian or Alpine ISO image of the current up to day packages.
 
The simplest equivalent is Debian's `live-build`, but without so many complications, 
just add packages, create the iso starting mechanisms, but without changing the nature 
of the base distro to be generated.

## About the project

The goal of **venenux-teaiso** if the testing (or verificatons) of new (or olders) packages 
and configurations (or dessired modifications) inside the current venenux live iso structure.

**Teaiso** is the iso generation tool of the **Tearch-linux** project at https://gitlab.com/tearch-linux.

If you are interesting to contribute please check [Contributing.md](Contributing.md) document.

#### Purpose of VenenuX teaiso

The "why?" question: the purpose is to simplify the creation of the iso 
in order to focus attention on the added packages and their integration 
respect the changes within the iso itself made by VenenuX. This means 
that the creator can dischard the error from the ISO build process.

As example: the new version of grub2 changes configurations so to test the venenux grub artwork 
we can just use teaiso, cos if where tested using normal `live-build` the differences 
in post iso commands will produce too many complex errors. So with `makeiso` the 
creator of the VenenuX ISO can test the new iso and whatch happened in that part 
because the process of the creation are more simplistic.

#### Technology involved

The very simple process are explained in [Teaiso-technology.md](Teaiso-technology.md) document.

## Where to start

The program is distribution agnostic.. just clone the repository and install as described.

1. Install the project, check the [Installation.md](Installation.md) documentation.
2. The manual usage is simple, check the [Manual-of-usage.md](Manual-of-usage.md)
3. After understand basics, check about the technology: [Teaiso-technology.md](Teaiso-technology.md)
4. We encourage you to read the [FAQ-and-notes.md](FAQ-and-notes.md)

## See also

* [starting-use-case.md](starting-use-case.md)
